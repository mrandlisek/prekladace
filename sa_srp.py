#!/usr/bin/python3

"""sa_sri-vypocet.py: implementacia syntaktickej analyzy ako syntaxou riadeneho interpretra (recursive descent interpreter)"""
__author__ = "Michal Vagac"
__email__ = "michal.vagac@gmail.com"

import la
import ast
import metaData


class SA_RDI:
    def __init__(self, f):
        self.f = f
        self.la = la.LA()
        self.metaData = metaData.metaData()

    def match(self, token):
        if self.token == token:
            self.token, self.lexeme = self.la.dalsi(self.f)
        else:
            raise Exception("chyba!")

    def scanto(self, synch):
        while self.token != la.Token.KONIEC and self.token not in synch:
            self.token, self.lexeme = self.la.dalsi(self.f)

    def checkinput(self, zac, nasl):
        if self.token not in zac:
            print(self.lexeme)
            raise Exception("Chyba")
            print("CHYBA!!!")
            self.scanto(zac + nasl)

    def zacni(self):
        self.token, self.lexeme = self.la.dalsi(self.f)
        ret = self.odpalTvorenieUzlov()
        if self.token != la.Token.KONIEC:
            raise Exception("chyba!")
        return ret

    def odpalTvorenieUzlov(self):
        zac = [la.Token.LZATVORKA, la.Token.KONSTANTA, la.Token.CYKLUS, la.Token.IDENTIFIKATOR]
        nasl = []
        uzly = ast.HlavnyUzol()
        self.checkinput(zac, nasl)
        if self.token in zac:
            while self.token != la.Token.KONIEC:
                if self.token == la.Token.CYKLUS:
                    self.match(self.token)
                    uzly.pridajUzol(self.cyklusVyraz())
                else:
                    uzly.pridajUzol(self.vyraz())
        return uzly.vratUzly()

    def vyraz(self):
        # nastav mnoziny zac a nasl pre tento neterminal
        zac = [la.Token.LZATVORKA, la.Token.KONSTANTA, la.Token.CYKLUS, la.Token.IDENTIFIKATOR]
        nasl = [la.Token.PZATVORKA, la.Token.KONIEC, la.Token.PLUS, la.Token.MINUS, la.Token.VACSIEROVNE,
                la.Token.MENSIEROVNE, la.Token.MENSIE, la.Token.VACSIE]
        self.checkinput(zac, nasl)
        if self.token in zac:
            u = self.clen()
            while self.token == la.Token.PLUS or self.token == la.Token.MINUS:
                u_novy = ast.UzolOperatora(self.token)
                self.match(self.token)
                u_novy.pridaj_lavy(u)
                u_novy.pridaj_pravy(self.clen())
                u = u_novy
            if self.token == la.Token.VACSIE or self.token == la.Token.MENSIE or self.token == la.Token.MENSIEROVNE or self.token == la.Token.VACSIEROVNE or self.token == la.Token.ROVNASAROVNASA:
                u_novy = ast.UzolOperatora(self.token)
                self.match(self.token)
                u_novy.pridaj_lavy(u)
                u_novy.pridaj_pravy(self.clen())
                u = u_novy
            if self.token == la.Token.MINUSMINUS or self.token == la.Token.PLUSPLUS:
                if self.token == la.Token.MINUSMINUS:
                    pom = la.Token.MINUS
                elif self.token == la.Token.PLUSPLUS:
                    pom = la.Token.PLUS
                u_novy = ast.UzolOperatora(pom)
                self.match(self.token)
                u_novy.pridaj_lavy(u)
                u_novy.pridaj_pravy(ast.UzolHodnoty(1))
                u = u_novy
            return u

    def clen(self):
        # nastav mnoziny zac a nasl pre tento neterminal
        zac = [la.Token.LZATVORKA, la.Token.KONSTANTA, la.Token.PLUSPLUS, la.Token.MINUSMINUS, la.Token.IDENTIFIKATOR]
        nasl = [la.Token.PZATVORKA, la.Token.KONIEC, la.Token.PLUS, la.Token.MINUS, la.Token.KRAT, la.Token.LOMENE,
                la.Token.VACSIE, la.Token.MENSIE, la.Token.ROVNASAROVNASA, la.Token.VACSIEROVNE, la.Token.MENSIEROVNE]
        self.checkinput(zac, nasl)
        if self.token in zac:
            u = self.cinitel()
            while self.token == la.Token.KRAT or self.token == la.Token.LOMENE:
                u_novy = ast.UzolOperatora(self.token)
                self.match(self.token)
                u_novy.pridaj_lavy(u)
                u_novy.pridaj_pravy(self.cinitel())
                u = u_novy
            return u

    def cinitel(self):
        # nastav mnoziny zac a nasl pre tento neterminal
        zac = [la.Token.LZATVORKA, la.Token.KONSTANTA, la.Token.IDENTIFIKATOR]
        nasl = [la.Token.PZATVORKA, la.Token.KONIEC, la.Token.PLUS, la.Token.MINUS, la.Token.KRAT, la.Token.LOMENE,
                la.Token.VACSIE, la.Token.MENSIE, la.Token.ROVNASAROVNASA, la.Token.MENSIEROVNE, la.Token.VACSIEROVNE]
        self.checkinput(zac, nasl)
        if self.token in zac:
            if self.token == la.Token.LZATVORKA:
                self.match(la.Token.LZATVORKA)
                u = self.vyraz()
                self.match(la.Token.PZATVORKA)
                return u
            elif self.token == la.Token.KONSTANTA or self.token == la.Token.IDENTIFIKATOR:
                if self.token == la.Token.IDENTIFIKATOR:
                    for val in self.metaData.getValue():
                        if self.lexeme in val:
                            h = val[1]
                    self.match(la.Token.IDENTIFIKATOR)
                else:
                    h = self.lexeme
                    self.match(la.Token.KONSTANTA)
                return ast.UzolHodnoty(h)
            else:
                raise Exception("chyba!")

    def cyklusVyraz(self):
        zac = [la.Token.LZATVORKA]
        nasl = [la.Token.KONSTANTA, la.Token.BODKOCIARKA, la.Token.PZATVORKA, la.Token.IDENTIFIKATOR]
        self.checkinput(zac, nasl)
        if self.token in zac:
            cyklus = ast.UzolCyklus()
            if self.token == la.Token.LZATVORKA:
                self.match(la.Token.LZATVORKA)
                if self.token == la.Token.IDENTIFIKATOR:
                    cyklus.cyklus_start(ast.UzolHodnoty(self.priradPrem(self.lexeme)))
                elif self.token == la.Token.BODKOCIARKA:
                    cyklus.cyklus_start(0)
                else:
                    raise Exception("chyba!")

                if self.token == la.Token.BODKOCIARKA:
                    self.match(self.token)
                    if self.token == la.Token.BODKOCIARKA:
                        cyklus.cyklus_operacia(0)
                    else:
                        cyklus.cyklus_operacia(self.vyraz())
                else:
                    raise Exception("chyba!")
                self.match(self.token)
                if self.token == la.Token.PZATVORKA:
                    cyklus.cyklus_koniec(0)
                    self.match(la.Token.PZATVORKA)
                    cyklus.cyklus_telo(self.cyklusTelo())
                elif self.token == la.Token.IDENTIFIKATOR:
                    cyklus.cyklus_koniec(self.vyraz())
                    self.match(self.token)
                    cyklus.cyklus_telo(self.cyklusTelo())
                else:
                    raise Exception("chyba!")
        else:
            raise Exception("chyba!")
        return cyklus

    def priradPrem(self, prem):
        self.match(self.token)
        if self.token == la.Token.ROVNASA:
            self.match(self.token)
            if self.token == la.Token.KONSTANTA:
                self.metaData.setValue(prem, self.lexeme)
                start = self.lexeme
                self.match(self.token)
                return start
            else:
                raise Exception("Chyba!")
        else:
            raise Exception("Chyba!")

    def cyklusTelo(self):
        zac = [la.Token.KLZATVORKA]
        nasl = [la.Token.KPZATVORKA, la.Token.KONSTANTA]
        self.checkinput(zac, nasl)
        pom = ast.HlavnyUzol()
        if self.token in zac:
            if self.token == la.Token.KLZATVORKA:
                self.match(self.token)
                while self.token != la.Token.KPZATVORKA:
                    if self.token == la.Token.KONSTANTA:
                        pom.pridajUzol(self.vyraz())
                    elif self.token == la.Token.CYKLUS:
                        self.match(self.token)
                        pom.pridajUzol(self.cyklusVyraz())
                self.match(la.Token.KPZATVORKA)
                return pom
        else:
            raise Exception("chyba!")


if __name__ == "__main__":
    f = open("vstup.txt", "r")
    s = SA_RDI(f)
    ast = s.zacni()

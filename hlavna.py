#!/usr/bin/python3

"""hlavna.py: implementacia syntaktickej analyzy ako syntaxou riadeneho interpretra (recursive descent interpreter)"""
__author__ = "Michal Vagac"
__email__ = "michal.vagac@gmail.com"

import struct

import la
import ast
import sa_srp
import avr
import gen


f = open("vstup.txt", "r")
s = sa_srp.SA_RDI(f)
ast = s.zacni()
gk = gen.GeneratorKodu()
gk.generuj_kod(ast)
gk.zobraz_asm()
print("")
gk.zobraz_hex()

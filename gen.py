from enum import Enum

import la
import ast
import avr


class Register(Enum):
    R16 = 16,
    R17 = 17,
    R18 = 18,
    R19 = 19,
    R20 = 20,

    def __lt__(self, other):
        return self.value[0] < other.value[0]


class GeneratorKodu:

    def __init__(self):
        self.registre = []
        for r in Register:
            self.registre.append(r)
        self.kod = []
        self.registerCyklu = ''
        self.genKoniec = 0

    def vezmi_reg(self):
        if len(self.registre) == 0:
            raise Exception("nedostatok registrov!")
        self.registre.sort()
        ret = self.registre[0]
        del self.registre[0]
        return ret

    def vrat_reg(self, r):
        self.registre.append(r)

    def generuj_kod(self, ast):
        for i in ast:
            vys = self.generuj(i)

    def generuj(self, u):
        if isinstance(u, ast.UzolHodnoty):
            r = self.vezmi_reg()
            self.kod.append(avr.Instr_LDI(r, int(u.lexem)))
            return r
        if isinstance(u, ast.UzolOperatora):
            # operator ma lavu a pravu vetvu; treba ich obe vyhodnotit
            tp = self.generuj(u.pravy)
            if self.genKoniec == 1:
                tl = self.registerCyklu
            else:
                tl = self.generuj(u.lavy)
            if u.operator == la.Token.PLUS:
                self.kod.append(avr.Instr_ADD(tl, tp))
                self.vrat_reg(tp)
                return tl
            if u.operator == la.Token.MINUS:
                self.kod.append(avr.Instr_SUB(tl, tp))
                self.vrat_reg(tp)
                return tl
            if u.operator == la.Token.VACSIE:
                self.kod.append(avr.Instr_VACSIE(self.registerCyklu, tp))
                # self.vrat_reg(tp)
                return tp
            if u.operator == la.Token.MENSIE:
                self.kod.append(avr.Instr_MENSIE(self.registerCyklu, tp))
                # self.vrat_reg(tp)
                return tp
            if u.operator == la.Token.MENSIEROVNE:
                self.kod.append(avr.Instr_MENSIEROVNE(self.registerCyklu, tp))
                # self.vrat_reg(tp)
                return tp
            if u.operator == la.Token.VACSIEROVNE:
                self.kod.append(avr.Instr_MENSIEROVNE(self.registerCyklu, tp))
                # self.vrat_reg(tp)
                return tp
        if isinstance(u, ast.UzolCyklus):
            self.generujCyklus(u)

    def generujCyklus(self, u):
        if u.cyklusStart == 0 and u.cyklusOperacia == 0 and u.cyklusKoniec == 0:
            #TODO nekonecny cyklus
            self.nekonecnyCyklus()
            if u.cyklusTelo:
                self.generuj_kod(u.cyklusTelo.uzol)
        else:
            self.registerCyklu = self.generuj(u.cyklusStart)
            if u.cyklusOperacia:
                 self.generuj(u.cyklusOperacia)
            if u.cyklusTelo:
              self.generuj_kod(u.cyklusTelo.uzol)
            if u.cyklusKoniec:
                self.genKoniec = 1
                self.generuj(u.cyklusKoniec)
                self.genKoniec = 0

    def nekonecnyCyklus(self):
        l = self.vezmi_reg()
        self.kod.append(avr.Instr_LDI(l, int(0)))
        p = self.vezmi_reg()
        self.kod.append(avr.Instr_LDI(p, int(0)))
        self.kod.append(avr.Instr_MENSIEROVNE(l, p))

    def zobraz_asm(self):
        for i in self.kod:
            print(i)

    def pocitaj_checksum(self, h):
        lsb_sucet = sum(bytes.fromhex(h)) & 255
        # doplnkovy kod (inverzny kod + 1)
        return format((((1 << 8) - 1) & (~lsb_sucet)) + 1, 'x')

    def zobraz_hex(self):
        # https://en.wikipedia.org/wiki/Intel_HEX

        # extended segment address
        poc_bajtov = "02"  # kedze adresa v data je 16 bitova, budu tam 2 bajty
        adresa = "0000"  # ignoruje sa; dame 0
        typ = "02"
        data = "0000"  # 16-bitova bazova adresa segmentu (je teda nasobena 16)
        spolu = poc_bajtov + adresa + typ + data
        print(":" + spolu + self.pocitaj_checksum(spolu))

        # data (instrukcie)
        adresa = "0000"
        typ = "00"
        data = ""
        poc_bajtov = 0
        for i in self.kod:
            opcode = i.opcode()
            for j in opcode:
                # data += format(j, 'x')
                data += "{0:0{1}x}".format(j, 2)
                poc_bajtov += 1
        spolu = "{0:0{1}x}".format(poc_bajtov, 2) + adresa + typ + data
        print(":" + spolu + self.pocitaj_checksum(spolu))

        # end of file
        poc_bajtov = "00"
        adresa = "0000"
        typ = "01"
        spolu = poc_bajtov + adresa + typ
        print(":" + spolu + self.pocitaj_checksum(spolu))

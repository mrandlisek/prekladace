#!/usr/bin/python3

"""pa.py: implementacia lexikalnej analyzy"""
__author__ = "Michal Vagac"
__email__ = "michal.vagac@gmail.com"

from enum import Enum


class Token(Enum):
    IDENTIFIKATOR = 1,
    KONSTANTA = 2,
    PLUS = 3,
    MINUS = 4,
    KRAT = 5,
    LOMENE = 6,
    LZATVORKA = 7,
    PZATVORKA = 8,
    KLZATVORKA = 9,
    KPZATVORKA = 10,
    BODKOCIARKA = 11,
    KONIEC = 12,
    CYKLUS = 13,
    VACSIE = 14,
    MENSIE = 15,
    ROVNASA = 16,
    VACSIEROVNE = 17,
    MENSIEROVNE = 18,
    ROVNASAROVNASA = 19,
    PLUSPLUS = 20,
    MINUSMINUS = 21


pis = lambda c: len(c)==1 and c.isalpha()
cis = lambda c: len(c)==1 and c.isnumeric()

class LA:
    def __init__(self):
        self.prechody = [[pis,cis,'+','-','*','/','(',')','$','{','}',';','>','<','=','ine', 'akc'],
                         #1
                         [  2,  4,  25,  26,  8,  9, 10, 11, 12, 13, 14, 15, 20, 21, 19,  0, None ],
                         #2
                         [  2,  2, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,  -3, None ],
                         #3
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,   0, Token.IDENTIFIKATOR ],
                         #4
                         [ -5,  4, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5,  -5, None ],
                         #5
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.KONSTANTA ],
                         #6
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,   0, Token.PLUS ],
                         #7
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.MINUS ],
                         #8
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.KRAT ],
                         #9
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.LOMENE ],
                         #10
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.LZATVORKA ],
                         #11
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.PZATVORKA ],
                         #12
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.KONIEC ],
                         #13
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.KLZATVORKA ],
                         #14
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.KPZATVORKA ],
                         #15
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,  0, Token.BODKOCIARKA ],
                         #16
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0,0, 0, 0,   0, Token.VACSIE ],
                         #17
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0,0, 0, 0 ,  0, Token.MENSIE ],
                         #18
                         [  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0,0, 0, 0 ,  0, Token.ROVNASA ],
                         #19
                         [-18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, -18, 24, 23, 22, 0, None],
                         #20
                         [-16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, -16, 24, 0, None],
                         #21
                         [-17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, -17, 17, 23, 0, None],
                         #22
                         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Token.ROVNASAROVNASA],
                         #23
                         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Token.MENSIEROVNE],
                         #24
                         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Token.VACSIEROVNE],
                         #25
                         [-6, -6, 27, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, 0, None],
                         #26
                         [-7, -7, -7, 28, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, -7, 0, None],
                         #27
                         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Token.PLUSPLUS],
                         #28
                         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Token.MINUSMINUS],
                         ]
        self.riadok = 1
        self.stlpec = 0

    def citajZnak(self, f):
        while True:
            ch = f.read(1)
            if not ch:
                # end of file
                ch = '$'
            if ch == '\n':
                self.riadok += 1
                self.stlpec = 0
            else:
                self.stlpec += 1
            if ch != ' ' and ch != '\n' and ch != '\r' and ch != '\t':
                break
        return ch

    def vyberPrechod(self, symbol):
        # return self.prechody.get(symbol)

        def isalambda(v):
            LAMBDA = lambda: 0
            return isinstance(v, type(LAMBDA)) and v.__name__ == LAMBDA.__name__

        stlpec = -1
        for i in range(0, len(self.prechody[0])):
            if self.prechody[0][i] == symbol:
                stlpec = i
                break
            elif isalambda(self.prechody[0][i]):
                if self.prechody[0][i](symbol):
                    stlpec = i
                    break
        if stlpec == -1:
            return None
        ret = []
        for i in range(0, len(self.prechody)):
            ret.append(self.prechody[i][stlpec])
        return ret

    def dalsi(self, f):
        stav = 1
        lexeme = ''
        while self.vyberPrechod('akc')[abs(stav)] is None:
            lookahead = f.tell()
            ch = self.citajZnak(f)
            lexeme += ch
            if self.vyberPrechod(ch) is not None:
                novystav = self.vyberPrechod(ch)[abs(stav)]
            else:
                novystav = self.vyberPrechod('ine')[abs(stav)]
            # prejdi na novy stav
            stav = novystav
            if stav == 0:
                raise Exception("chyba!")
        if stav < 0:
            f.seek(lookahead)
            self.stlpec -= 1
            lexeme = lexeme[:-1]
        if lexeme == 'for' and self.vyberPrechod('akc')[abs(stav)] == Token.IDENTIFIKATOR:
            return Token.CYKLUS, lexeme
        else:
            return self.vyberPrechod('akc')[abs(stav)], lexeme

if __name__ == "__main__":
    f = open("vstup.txt","r")
    la = LA()
    while True:
        token, lexeme = la.dalsi(f)
        print(token, lexeme)
        if token == Token.KONIEC:
            break

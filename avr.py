class Instr_LDI():
    def __init__(self, d, K):
        if d.value[0] < 16:
            raise Exception("LDI dokaze pracovat len s registrami R16-R31!")
        self.d = d
        self.K = K

    def opcode(self):
        instr = 0b1110000000000000
        Kl = (self.K & 0b11110000) << 4
        d2 = (self.d.value[0] - 16) << 4
        K2 = (self.K & 0b00001111)
        ret = instr | Kl | d2 | K2
        return ret.to_bytes(2, byteorder='little', signed=False)

    def __repr__(self):
        return "LDI " + str(self.d.name) + "," + str(self.K)


class Instr_ADD():
    def __init__(self, d, r):
        self.d = d
        self.r = r

    def opcode(self):
        instr = 0b0000110000000000
        r1 = (self.r.value[0] & 0b00010000) << 5
        r2 = (self.r.value[0] & 0b00001111)
        d1 = (self.d.value[0] & 0b00011111) << 4
        ret = instr | r1 | r2 | d1
        return ret.to_bytes(2, byteorder='little', signed=False)

    def __repr__(self):
        return "ADD " + str(self.d.name) + "," + str(self.r.name)


class Instr_SUB():
    def __init__(self, d, r):
        self.d = d
        self.r = r

    def opcode(self):
        instr = 0b0001100000000000
        r1 = (self.r.value[0] & 0b00010000) << 5
        r2 = (self.r.value[0] & 0b00001111)
        d1 = (self.d.value[0] & 0b00011111) << 4
        ret = instr | r1 | r2 | d1
        return ret.to_bytes(2, byteorder='little', signed=False)

    def __repr__(self):
        return "SUB " + str(self.d.name) + "," + str(self.r.name)


class Instr_VACSIE():
    def __init__(self, d, r):
        self.d = d
        self.r = r

    def opcode(self):
        instr = 0b1111000000000100
        r1 = (self.r.value[0] & 0b00010000) << 5
        r2 = (self.r.value[0] & 0b00001111)
        d1 = (self.d.value[0] & 0b00011111) << 4
        ret = instr | r1 | r2 | d1
        return ret.to_bytes(2, byteorder='little', signed=False)

    def __repr__(self):
        return "CP " + str(self.d.name) + "," + str(self.r.name) + " BRLT"


class Instr_MENSIE():
    def __init__(self, d, r):
        self.d = d
        self.r = r

    def opcode(self):
        instr = 0b1111000000000100
        r1 = (self.r.value[0] & 0b00010000) << 5
        r2 = (self.r.value[0] & 0b00001111)
        d1 = (self.d.value[0] & 0b00011111) << 4
        ret = instr | r1 | r2 | d1
        return ret.to_bytes(2, byteorder='little', signed=False)

    def __repr__(self):
        return "CP " + str(self.r.name) + "," + str(self.d.name) + " BRLT"


class Instr_VACSIEROVNE():
    def __init__(self, d, r):
        self.d = d
        self.r = r

    def opcode(self):
        instr = 0b1111010000000001
        r1 = (self.r.value[0] & 0b00010000) << 5
        r2 = (self.r.value[0] & 0b00001111)
        d1 = (self.d.value[0] & 0b00011111) << 4
        ret = instr | r1 | r2 | d1
        return ret.to_bytes(2, byteorder='little', signed=False)

    def __repr__(self):
            return "CP " + str(self.d.name) + "," + str(self.r.name) + " BRNE"


class Instr_MENSIEROVNE():
    def __init__(self, d, r):
        self.d = d
        self.r = r

    def opcode(self):
        instr = 0b1111010000000001
        r1 = (self.r.value[0] & 0b00010000) << 5
        r2 = (self.r.value[0] & 0b00001111)
        d1 = (self.d.value[0] & 0b00011111) << 4
        ret = instr | r1 | r2 | d1
        return ret.to_bytes(2, byteorder='little', signed=False)

    def __repr__(self):
            return "CP " + str(self.r.name) + "," + str(self.d.name) + " BRNE"

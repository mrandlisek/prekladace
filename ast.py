
class Uzol:
    def __init__(self):
        None

class UzolOperatora(Uzol):
    def __init__(self, operator):
        self.operator = operator

    def pridaj_lavy(self, lavy):
        self.lavy = lavy

    def pridaj_pravy(self, pravy):
        self.pravy = pravy

class UzolHodnoty(Uzol):
    def __init__(self, lexem):
        self.lexem = lexem

class UzolCyklus(Uzol):

    def cyklus_start(self, cyklusStart):
        self.cyklusStart = cyklusStart

    def cyklus_koniec(self, cyklusKoniec):
        self.cyklusKoniec = cyklusKoniec

    def cyklus_operacia(self, cyklusOperacia):
        self.cyklusOperacia = cyklusOperacia

    def cyklus_telo(self, cyklusTelo):
        self.cyklusTelo = cyklusTelo

class HlavnyUzol(Uzol):
    def __init__(self):
        self.uzol = []
    def pridajUzol(self, uzol):
        self.uzol.append(uzol)
    def vratUzly(self):
        return self.uzol

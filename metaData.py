class metaData:
    def __init__(self):
        self.metaData = []

    def getValue(self):
        return self.metaData

    def setValue(self, name, value):
        self.metaData.append([name, value])